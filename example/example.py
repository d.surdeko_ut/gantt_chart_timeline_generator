# Imports (these are necessary)
import sys
import matplotlib.pyplot as plt
# Add module to path and import it (replace '../' with the path to the
# gantt_timeline_generator.py)
sys.path.insert(0, '../')
# Import gantt_timeline_generator
import gantt_timeline_generator as gtg

# List of dates to be marked with vertical lines on the chart
# You can use this as a template. All the fields are expected
# to be filled in.
dates = [
        {'date': '2021-02-01',
         'date_format': '%Y-%m-%d',
         'label': ['text', 'meeting'],
         'alpha': 1,
         'color': 'aqua',
         'linestyle': '-',
         'linewidth': 2,
        },
        {'date': '2021-05-15',
         'date_format': '%Y-%m-%d',
         'label': ['text', 'pre-production units'],
         'alpha': 1,
         'color': 'darkorchid',
         'linestyle': '-',
         'linewidth': 2,
        }
        ]

# Colors for departments
# You can assign different "qualifiers" to your tasks, e.g.
# teams that work on a given part of a project. Then you can
# assign colors to these qualifiers. If you do not want to
# display qualifiers in the legend, set 'ShowCategoryLegend'
# in the customization dictionary to False. This way you will
# be able to arbitrarily change the colors of the bars in the
# plot, without the need to assign any meaningful qualifiers
# to the tasks.
legend_colors = {
        'Design': 'royalblue',
        'R&D': 'orangered',
        'QA': 'gold',
        'Production': 'green',
        'Sales & marketing': 'chocolate',
        }

# Defining for which tasks progress should be shown separately.
# Note that this works with 'ProgressSeperateDefaul' from the
# customization dictionary. If you do not specify certain
# tasks here, a default value given by the aforementioned
# parameter will be assigned to them.
separate_progress = {
        'Evaluation': True,
        }

# Dictionary of customization values
# You do not have to pass a full customization dictionary to
# the function - you can choose to adjust only some of the
# default parameters. For more detail about the available
# customization settings check the git page of the project
# or type help(gtg.gantt_timeline) in an interactive
# Python session (after importing the module as shown at the
# beginning of this script).
customization = {
        'PlotXStepMajor': {'months': 2},
        'PlotXStepMinor': {'months': 1},
        'ShowProgress': True,
        'ProgressSeparate': separate_progress,
        'ShowCategoryLegend': True,
        'LegendColors': legend_colors,
        'OutputDateFormat': '%d %b %y',
        'DateLine': dates,
        }

# Data input path, input data date format and delimiter in the CSV file
# Set data input path. It can be relative or absolute. The path should
# include the name of the input file together with the extension.
# You can check how an input file should look like by taking a look
# at the input file for this script. For more information about the
# structure of the input file check the git page of the project or type
# help(gtg.gantt_timeline) in an interactive Python session (after
# importing the module as shown at the beginning of this script).
input_path = './example_data.csv'
# Set the format of the dates in the input file (must be consistent!).
# Use strftime strings (https://strftime.org/).
input_date_format = '%d-%m-%Y'
# Set the delimiter used in your csv file. This is necessary for
# appropriate parsing of the input file.
delimiter = ','
# Defining if the tasks should be ordered by starting date (if True) or if
# they should show up in the plot in the order they were listed in the input
# data (if False).
order_by_date = True;

# Preparing the plot
fig = gtg.gantt_timeline(input_path, delimiter=delimiter,
        input_date_format=input_date_format, order_by_date=order_by_date,
        customization=customization)
# Saving and displaying the plot in the current directoryk
plt.savefig('example.svg')
plt.show()
