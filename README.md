Gantt Chart Timeline Generator
==============================

## Objective
Gantt Chart Timeline Generator is a simple Python module that allows quick and easy creation of clean-looking Gantt chart timeline plots for the purpose of e.g. project planning. While it is not a professional project, it may be useful for those who need a simple tool that just gets the job done.

![Example chart](example/example.svg)

## Installation

### Requirements

Dependencies: matplotlib, pandas. The tool was tested and is confirmed to work with Python 3.9.7, matplotlib 3.5.1 and pandas 1.4.1.

### Installation

1. Clone this repository to any directory you see fit.
2. Place the gantt_timeline_generator.py file in a directory at your system path or, in your Python script, add the directory where gantt_timeline_generator.py is located to your system path. The latter can be done by inserting the following commands at the beginning of your Python script:
> 
> ```
> import sys
> import matplotlib.pyplot as plt
> # Replace '../' with the path to the directory
> # contatining gantt_timeline_generator.py
> sys.path.insert(0, '../')
> import gantt_timeline_generator as gtg
> ```

For more information on how to use the tool, check the [example](example/example.py) and the [manual](MANUAL.md).

## Basics of operation and features

The tool reads data from a .csv file, which should contain at least the names of tasks to be presented in the chart as well as starting and ending dates for each of those tasks. The main user-facing function assumes sane defaults, thus a decent output can be achieved simply by passing to the function the path to the data and the format of the dates in the .csv file. However, despite being rather simplistic, Gantt Chart Timeline Generator offers quite a lot of customization options and features.

As an example, one of such features is that tasks that are not continuous can be easily represented in the chart - as long as the names of the task are the same, the bars representing these tasks will be displayed in the same row. Furthermore, progress for the tasks can be presented in the chart. Notably, this feature works with the aforementioned multi-bar rows: the displayed progress can be either a summary of the progress of all these tasks or it can be shown for each of them separately.

The progress can be represented in two ways: through difference in the opacity of the completed and the to-do parts of the tasks as well as through display of the percentage of completion in the chart. These methods of presenting the information can be used independently of each other. In case of non-continous tasks, the user can decide, for each of these tasks separately, how to measure their progress. In turn this affects how the progress information is displayed. If the progress is supposed to be a summary of completion of the whole non-continous task (a collection of tasks with the same name), then the opacity is changed in such a way, that all the bars in a given row act as a single progress bar. Additionally, the completion information expressed in percentage is displayed right behind the last bar in a given row. However, if the progress for tasks with the same name is supposed to be considered separately, then the opacity is tweaked such that each bar represents the progress of the individual, singular task and the percentage of completion for each of these tasks is displayed on the bars.

The tool also supports adjustment of the colors of the bars. To utilize this feature, the user must assign group labels to each of the tasks and then assign a color to each of these groups. Importantly, the groups *can, but do not have to*, be displayed in the chart legend. Such implementation gives the user flexibility of either adding more information to the plot through usage of colors or of adjusting the colors simply for the visual effect.

Finally, the last important feature of the tool is that an arbitrary number of customizable, vertical lines can be added to the plot. These lines can be used to mark important dates/events/milestones. The user can decide whether entries for these lines should be present in the chart legend or not.

Despite relative simplicity, the tool allows the user to adjust parameters such as colors, sizes of elements, date formats, labels of the axes, chart title, background grid and more. This customization is done through a dictionary of customization options that is passed to the main user-facing function.

## Other information

The module exposes a single function to the user: gantt_timeline. Information about the function and all its input variables is given in the [MANUAL.md](MANUAL.md).

Note that this tool is not a professional project. The support may be limited and some choices regarding coding practices are non-standard (deliberate choices of the developer). That being said, feel free to contribute to the project by proposing ideas, submitting bug reports, feature requests, pull requests etc.
