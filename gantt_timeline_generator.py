# Copyright: 2022 Dawid Surdeko
# E-mail: d.surdeko@utwente.nl

# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


# Imports
from collections import Counter
import copy
import datetime as dt
from dateutil.relativedelta import relativedelta
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import matplotlib.pyplot as plt
import pandas as pd

# Functions

def _unique(input_list):

    """A function that returns unique values of a list in the same order as in
    the input list.

    Input:

        input_list: a list with repeated elements.

    Output: a list of unique elements in the input_list in the same order as
    in the input_list."""

    return [*Counter(input_list)]


def _time_delta(data):

    """Read input data dictionary and convert it into a time delta object.

    Input:

        data: a dictionary defining the time delta; not all the fields must
        be given, the missing ones will be assumed to be 0;
        type: dictionary; allowed fields:

            'years': number of years; type: float;

            'months': number of months; type: float;

            'weeks': number of weeks; type: float;

            'days': number of days; type: float;

            'hours': number of hours; type: float;

            'minutes': number of minutes; type: float;

            'seconds': number of seconds; type: float;

            'microseconds': number of microseconds; type: float;
    """

    # Preparing a default dictionary corresponding to time delta of 0. It will
    # be used to prepare the actual dictionary that will be parsed.
    delta = {
            'years': 0,
            'months': 0,
            'weeks': 0,
            'days': 0,
            'hours': 0,
            'minutes': 0,
            'seconds': 0,
            'microseconds': 0,
            }

    # Reading the keys of the input dictionary and the default dictionary
    keys = list(data.keys())
    keys_default = list(delta.keys())

    # Replacing the values in the default dictionary with appropriate values
    # from the input dictionary
    for key in keys:

        if key in keys_default:

            delta[key] = data[key]

    # Creating the time delta object
    delta = relativedelta(years = delta['years'], months = delta['months'],
            weeks = delta['weeks'], days = delta['days'],
            hours = delta['hours'], minute = delta['minutes'],
            seconds = delta['seconds'], microseconds = delta['microseconds'])

    return delta


def _read_input(input_path, delimiter=',', input_date_format='%Y-%m-%d',
                order_by_date=True):

    """Read input data from a .csv file, sort by task start date (if required),
    remove NaN values for progress, add columns of data required for further
    processing and return data for further work.

    The input data must contain the following columns:

        Task: a column containing names of the tasks to be displayed
        on the Gantt chart;

        Start: starting date of a task;

        End: end date of a task;

    Optional columns:

        Legend: label/category of a given element for display in the
        legend of the chart;

        Progress: level of completion of a task (0 - 1);

    Input:

        input_path: path to the .csv file containing the data (required);
        type: string;
        
        delimiter: delimiter for the .csv file;
        type: string; default: ',';

        input_date_format: format of data strings in the input data file,
        given using a strftime string (https://strftime.org/);
        type: string; default: '%Y-%m-%d';

        order_by_date: boolean value defining if the tasks should be ordered
        by starting date (if True) or if they should show up in the plot in the
        order they were listed in the input data (if False);
        type: boolean; default: True;

    Output:

        data: a pandas data frame containing the data for processing, sorted
        by start date;
        type: pandas data frame;

        order: a list of task names, ordered in the order in which they should
        be displayed on the plot, from the top to the bottom;
        type: list of strings;
    """
    
    # Read data and convert dates to pandas datetime objects
    data = pd.read_csv(input_path, delimiter = delimiter)
    data['Start'] = pd.to_datetime(data['Start'], format = input_date_format)
    data['End'] = pd.to_datetime(data['End'], format = input_date_format)

    # Replace NaNs with zeros.
    if 'Progress' in list(data.keys()):

        data['Progress'] = data['Progress'].fillna(0)

    # Check if 'Legend' column exists, otherwise create it. If it exists but
    # contains 'NaN' values, fill them in with the 'default' value.
    if 'Legend' not in list(data.keys()):

        data.assign(Legend = data.shape[0]*['default'])

    else:

        data['Legend'] = data['Legend'].fillna('default')

    # Sort by task start date if desired
    if order_by_date:

        data = data.sort_values(by = 'Start')

    # Find starting date of the project
    project_start_date = data['Start'].min()
    # Add columns containing numbers of days from the start of the project
    # to the starting dates of tasks, from the start of the project to the
    # ending dates of tasks and numbers of days of duration of the tasks
    data['Project day start'] = (data['Start'] - project_start_date).dt.days
    data['Project day end'] = (data['End'] - project_start_date).dt.days
    data['Task duration'] = (data['Project day end'] - \
            data['Project day start']).astype('timedelta64[D]')
    data['Task duration'] = data['Task duration'] + dt.timedelta(days = 1)
    # Save order of elements
    order = _unique(data['Task'].tolist())

    return [data, order]


def _plot_gantt(data, order, tick_reference_date=None, customization={}):

    """Plot a Gantt chart from a data frame, according to parameters listed in
    the customization dictionary. Sane default values are used, thus one does
    not need to provide all the parameters to obtain a reasonable plot.

    Input:

        data: a pandas data frame containing the data for the Gantt chart;
        columns:

            Task: a column containing names of the tasks to be displayed
            on the Gantt chart; obligatory column;

            Start: starting date of a task; obligatory column;

            End: end date of a task; obligatory column;

            Project day start: time of starting the task expressed in days from
            the start of the project; obligatory column;

            Project day end: time of ending the task expressed in days from the
            start of the project; obligatory column;

            Task duration: duration of the task expressed in days; obligatory
            column;

            Legend: label/category of a given element for display in the
            legend of the chart; obligatory column;

            Progress: level of completion of a task (0 - 1); optional column;

        order: a list of task names, ordered in the order in which they should
        be displayed on the plot, from the top to the bottom;
        type: list of strings;

        tick_reference_date: custom starting date for calculating the
        positions of the ticks; this allows to e.g. display the ticks always
        on the first day of the month, rather than every month from the
        beginning of the first task; note that the plot itself will still
        start at the beginning date of the first task - earlier ticks will
        not be displayed; the date must be earlier or the same as the
        earliest beginning date in the data set; if None, the reference date
        will be set to the beginning date of the first task;
        type: datetime object or None; default: None;

        customization: a dictionary listing all the settings for the chart;
        default: {}; available keys:

            ChartWidth: width of the chart;
            type: float; default: 20;

            ChartHeight: height of the chart;
            type: float; default: 10;

            BarGap: distance between the bars (in y direction);
            type: float; default: 0.4;

            BarHeight: height of the bars;
            type: float; default: 4;

            PlotXStepMajor: step on the x axis for the major ticks, given as
            a dictionary; not all the fields must be given, the missing ones
            will be assumed to be 0;
            type: dictionary; default: {'days': 14}; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;
            
                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            PlotXStepMinor: step on the x axis for the minor ticks, given as
            a dictionary; not all the fields must be given, the missing ones
            will be assumed to be 0; if None, no minor ticks will be displayed;
            type: dictionary or None; default: {'days': 7}; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;

                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            PlotXStepMinorRelativeThreshold: if set to None, minor ticks are
            calculated irrespectively of the major ones, as defined in
            PlotXStepMinor; if set to a custom dictionary value, then the
            minor ticks are calculated with respect to the preceding major
            tick, i.e. the initial date is set to be that of the preceding
            major tick and the minor steps (defined through PlotXStepMinor)
            are made as many times as possible, until the date represented by
            the next major tick is reached; at that point the date of the new
            major tick is taken as the new starting point for the next minor
            ticks; the procedure repeats until the final date is reached (the
            threshold apply to it as well); minor ticks preceding the first
            major tick are also calculated according to the rules, with the
            first one starting not earlier than the earliest starting date
            in the data set; this allows e.g. displaying a minor tick every
            week of the month, starting at the beginning of the month; the
            dictionary enables customization of the number of minor ticks to
            be displayed by defining an upper threshold for the minor ticks
            to be displayed; the time delta defined by the dictionary is
            subtracted from the date represented by the next major tick or
            the final date in the set; if the date for the new minor tick is
            earlier than or the same as that calculated date, the tick will
            be displayed; otherwise it will be omitted; this allows to e.g.
            display a minor tick each 15th of the month, without a tick on
            29th; not all the fields of the dictionary must be given, the
            missing ones will be assumed to be 0 (note that an empty
            dictionary might be passed - in such a case all the minor ticks
            will be displayed, with the threshold being equal to the date of
            the next major tick);
            type: dictionary; default: None; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;
            
                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            XAxisLabel: label for the x axis, None if not to be displayed;
            type: string or None; default: None;

            XAxisLabelSize: size of the x axis label if given;
            type: any recognized by matplotlib; default: 32;

            YAxisLabel: label for the y axis, None if not to be displayed;
            type: string or None; default: None;

            YAxisLabelSize: size of the y axis label if given;
            type: any recognized by matplotlib; default: 32;

            TickSize: size of the tick labels on both axes;
            type: any recognized by matplotlib; default: 24;

            TickRotation: rotation of the tick labels on x axis;
            type: any recognized by matplotlib; default: 60;

            ChartTitle: title of the chart, None if not to be displayed;
            type: string or None; default: None;

            ChartTitleSize: size of the title of the chart, if given;
            type: any recognized by matplotlib; default: 34;

            ChartTitleRaise: a parameter that defines how high the chart title
            is raised above the plot;
            type: float; default: 1.03;

            ShowProgress: decide whether to display the progress of the task,
            both by changing the opacity of the completed tasks as well as
            displaying the value of progress in %;
            type: bool; default: False;

            ProgressSeparateDefault: if progress displayed and there are
            multiple tasks with the same label, define whether by default
            the progress for each of them should be displayed separately or
            considering them all together; in the latter case the total
            progress will be calculated from the individual values of progress
            of each of the task;
            type: bool; default: False;

            ProgressSeparate: if progress displayed and there are multiple
            tasks with the same label, define for each label whether to display
            the progress for individual tasks separately or considering them
            all together; in the latter case the total progress will be
            calculated from the individual values of progress of each of the
            task; default behaviour can be set with ProgressSeparateDefault;
            this way only exceptions can be given through this parameter;
            type: dict the keys of which are task labels and the value is bool
            defining whether progress of individual tasks using the same label
            should be considered separately (True) or should the grouped
            progress be displayed (False); default: as set through the
            customization dict variable ProgressSeparateDefault;

            ProgressFontAlpha: opacity for the font of the text informing about
            task progress;
            type: float (0 - 1); default: 1;

            ProgressFontColor: color of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'black';

            ProgressFontFamily: family of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'serif';

            ProgressFontSize: size of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 18;

            ProgressFontStyle: style of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'normal';
            
            ProgressFontWeight: weight of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'normal';

            OpacityProgressDone: opacity of the parts of the bars that
            reperesent parts of the tasks that have been completed;
            type: float (0 - 1); default: 1;

            OpacityProgressToDo: opacity of the parts of the bars that
            represent parts of the tasks that are yet to be completed;
            type: float(0 - 1); default: 0.5;

            ShowCategoryLegend: decide whether to display a legend based on
            categories assigned to tasks; works only if a 'Legend' column
            was provided;
            type: bool; default: False;

            LegendLocationAnchor: location (in y) of the bounding box for the
            legend on the chart;
            type: any recognized by matplotlib; default: 1.0085;

            LegendFontSize: size of the font in the legend;
            type: any recognized by matplotlib; default: 24;

            LegendColors: a dictionary, the keys of which are categories
            that can be assigned to tasks and the values of which are colors
            of bars (any format recognized by matplotlib); note that this
            variable may be used for arbitrary designation of colors of bars
            without displaying the legend;
            type: dictionary of colors (any format recognized by matplotlib);
            default: dictionary of all the legend labels + 'default', with
            the color "royalblue" assigned to all of them;

            LegendBackgroundColor: color of the background of the legend;
            type: any recognized by matplotlib; default: 'snow';

            ChartColorAround: the color of the background around the chart;
            type: any recognized by matplotlib; default: 'snow';

            ChartColorBackground: the color of the background of the chart
            (plotting field);
            type: any recognized by matplotlib; default: 'snow';

            ChartColor: the color of the fonts of the chart, spines, ticks,
            tick labels, axes' labels, legend elements etc.;
            type: any recognized by matplotlib; default: 'black';

            ShowGrid: define whether a grid should be displayed on the chart;
            type: bool; default: True;

            GridColor: color of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: 'gainsboro';

            GridAlpha: opacity of the grid lines;
            type: float (0 - 1); default: 0.5;

            GridLineStyle: style of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: '-';

            GridLineWidth: width of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: 1;

            GridWhich: choice of grid lines with respect to ticks;
            type: string ('major', 'minor', 'both'); default: 'major';

            GridAxis: choice of grid lines (horizontal, vertical, both);
            type: string ('both', 'x', 'y'); default: 'x';

            ProgressTogetherPosition: horizontal gap between the bar and the
            text describing the progress of a task in days;
            type: float; default: 1.5;

            OutputDateFormat: format of data strings to be used in the chart,
            given using a strftime string (https://strftime.org/);
            type: string; default: '%d %B';

            DateLine: a list of dictionaries detailing the vertical lines to be
            displayed on the chart;
            type: dictionary; default: {}; allowed fields:

                'date': a date to mark on the chart with a vertical line; if
                "today" is passed as an argument, the date at which the script
                is run will be used; otherwise, it can be any string that can
                be parsed using a strftime string given in 'date_format' field;
                type: string;

                'date_format': format for the date given in 'date', given
                using a strftime string (https://strftime.org/);
                type: string;

                'label': a two element list that defines if and how the line
                should be represented in the legend; the first element can be
                'text' or 'format' - it defines whether the second field
                should be interpreted as plain text or as a format in which the
                date should be displayed in the legend; the second string is
                either text label to be displayed in the legend or a format
                for the date to be shown; if a given vertical line should not
                be represented in the legend, None is passed instead of the
                described list;
                type: 2-element list of strings or None;

                'alpha': opacity of the line to be plotted;
                type: float (0 - 1);

                'color': color of the line to be plotted;
                type: any recognized by matplotlib;

                'linestyle': line style of the line to be plotted;
                type: any recognized by matplotlib; default = '-';

                'linewidth': width of the line of the line to be plotted;
                type: any recognized by matplotlib; default = 2;

            InvisibleSpines: list of chart spines that should be hidden;
            type: list of strings ('top', 'bottom', 'left', 'right');
            default: ['top', 'left', 'right'].

        Output: 

            fig: matplotlib figure with the Gantt chart;
            type: matplotlib figure.
    """
    
    # Dictionary of default customization values
    customization_default = {
            'ChartWidth': 20,
            'ChartHeight': 10,
            'BarGap': 0.4,
            'BarHeight': 4,
            'PlotXStepMajor': {'days': 14},
            'PlotXStepMinor': {'days': 7},
            'PlotXStepMinorRelativeThreshold': None,
            'XAxisLabel': None,
            'XAxisLabelSize': 32,
            'YAxisLabel': None,
            'YAxisLabelSize': 32,
            'TickSize': 24,
            'TickRotation': 60,
            'ChartTitle': None,
            'ChartTitleSize': 34,
            'ChartTitleRaise': 1.03,
            'ShowProgress': False,
            'ProgressSeparateDefault': False,
            'ProgressFontAlpha': 1,
            'ProgressFontColor': 'black',
            'ProgressFontFamily': 'serif',
            'ProgressFontSize': 18,
            'ProgressFontStyle': 'normal',
            'ProgressFontWeight': 'normal',
            'OpacityProgressDone': float(1.0),
            'OpacityProgressToDo': 0.5,
            'ShowCategoryLegend': False,
            'LegendLocationAnchor': 1.0085,
            'LegendFontSize': 24,
            'LegendColors': {key: 'royalblue' for key in data['Legend']},
            'LegendBackgroundColor': 'snow',
            'ChartColorAround': 'snow',
            'ChartColorBackground': 'snow',
            'ChartColor': 'black',
            'ShowGrid': True,
            'GridColor': 'gainsboro',
            'GridAlpha': 0.5,
            'GridLineStyle': '-',
            'GridLineWidth': 1,
            'GridWhich': 'major',
            'GridAxis': 'x',
            'ProgressTogetherPosition': 1.5,
            'OutputDateFormat': '%d %B',
            'DateLine': [],
            'InvisibleSpines': ['top', 'left', 'right'],
            }

    try:

        customization_default['ProgressSeparate'] = {key:
                customization['ProgressSeparateDefault']
                for key in data['Task']}

    except:

        customization_default['ProgressSeparate'] = {key:
                customization_default['ProgressSeparateDefault']
                for key in data['Task']}

    customization_default['LegendColors']['default'] = 'black'
   
    # Check if the user defined all customization options. Assume default
    # values for those, which were not declared.
    user_dict_keys = list(customization.keys())
    default_dict_keys = list(customization_default.keys())

    for key in default_dict_keys:

        if key not in user_dict_keys:

            customization[key] = customization_default[key]

    # Add to customization dictionary a value that determines whether
    # progress for multiple tasks with the same label should be considered
    # separately or together
    for key in data['Task']:

        customization_default['ProgressSeparate'] = {key: 
                customization_default['ProgressSeparateDefault'] for key in
                data['Task']},
        
        if key not in list(customization['ProgressSeparate'].keys()):

            customization['ProgressSeparate'][key] = \
                    customization['ProgressSeparateDefault']

    # Figure for plotting
    fig, ax = plt.subplots(figsize = (customization['ChartWidth'],
        customization['ChartHeight']))
    # Group rows using the same label and process them into a list for the
    # purpose of displaying them together.
    data_list = list(data.groupby('Task'))
    # Create a list of task labels
    tasks = [task[0] for task in data_list]

    # Check if a column "Progress" is present in the data frame. If not, switch
    # the progress display to False and add this column with progress for all
    # the tasks set to 1.
    if 'Progress' not in list(data.keys()):

        data.assign(Progress = data.shape[0]*[1])

    # Calculate step in y direction for plotting the bars
    bar_step = customization['BarGap'] + customization['BarHeight']
    y_positions = []

    # Iterate over grouped tasks
    for task_iter in range(len(data_list)):

        # Select task
        task = data_list[task_iter]
        # Select the data
        data_frame = task[1]
        # Find y position of the task on the plot
        y_position = order.index(task[0])*bar_step
        y_positions.append(y_position + customization['BarHeight']/2)

        # Check if progress for tasks with the same label should be considered
        # separately or together with all the other tasks with the same name
        #
        # If progress for tasks with the same label should be considered
        # separately
        if customization['ShowProgress'] and 'Progress' in list(data.keys()):

            if customization['ProgressSeparate'][task[0]]:

                # Prepare data for plotting
                duration_completed = task[1]['Task duration']* \
                        task[1]['Progress']
                to_plot = list(zip(task[1]['Start'], duration_completed))
                to_plot_2 = list(zip(task[1]['Start'] + duration_completed,
                    task[1]['Task duration'] - duration_completed))
                to_plot = to_plot + to_plot_2
                # Prepare a list of colors and for plotting
                colors = 2*[customization['LegendColors'][item] for item in 
                        task[1]['Legend']]
                # Prepare a list of transparency values
                alpha = [customization['OpacityProgressDone']]*task[1].shape[0]
                alpha = alpha + \
                        [customization['OpacityProgressToDo']]*task[1].shape[0]
                # Plot the bars
                ax.broken_barh(to_plot, [y_position,
                    customization['BarHeight']],
                        facecolors = colors, alpha = alpha)

                # Displaying the progress
                for item in range(len(task[1]['End'])):

                    ax.text(task[1]['Start'].iloc[item] +
                        task[1]['Task duration'].iloc[item]/2,
                        y_position + customization['BarHeight']/2,
                        f"{int(round(task[1]['Progress'].iloc[item]*100, 0))}%",
                        va = "center", ha = "center", 
                        alpha = customization['ProgressFontAlpha'],
                        color = customization['ProgressFontColor'],
                        fontfamily = customization['ProgressFontFamily'],
                        fontsize = customization['ProgressFontSize'],
                        fontstyle = customization['ProgressFontStyle'],
                        fontweight = customization['ProgressFontWeight'])

            # If progress for tasks with the same label should be considered
            # together with all the other tasks with the same name
            elif not customization['ProgressSeparate'][task[0]]:

                # Calculate total length of the tasks with the same label
                total_length = sum(task[1]['Task duration'], dt.timedelta())
                # Calculate length corresponding to the progress
                progress_length = sum(task[1]['Progress']*
                        task[1]['Task duration'], dt.timedelta())
                # Calculate progress
                total_progress = progress_length/total_length
                
                # Find the element where the "completed" part ends
                partial_sum = task[1].iloc[0]['Task duration']
                completion_iter = 0

                while partial_sum < progress_length:

                    completion_iter = completion_iter + 1
                    partial_sum = partial_sum + task[1].iloc[completion_iter]\
                            ['Task duration']

                # Calculate the length of the task, where the "completed" part
                # ends, that should be marked as completed
                progress_transition = \
                        task[1].iloc[completion_iter]['Task duration'] - \
                        partial_sum + progress_length
                # Prepare data for plotting
                to_plot = list(zip(task[1]['Start'], task[1]['Task duration']))
                del to_plot[completion_iter]
                to_plot.append((task[1].iloc[completion_iter]['Start'],
                    progress_transition))
                to_plot.append((task[1].iloc[completion_iter]['Start'] +
                    progress_transition, 
                    task[1].iloc[completion_iter]['Task duration'] - 
                    progress_transition))
                # Prepare a list of colors and for plotting
                colors = [customization['LegendColors'][item] for item in 
                        task[1]['Legend']]
                del colors[completion_iter]
                colors = colors + 2*[customization['LegendColors'][
                    task[1].iloc[completion_iter]['Legend']]]
                # Prepare a list of transparency values
                alpha = [customization['OpacityProgressDone']]*\
                        (completion_iter) + \
                        [customization['OpacityProgressToDo']]*\
                        (task[1].shape[0] - completion_iter - 1)
                alpha = alpha + [customization['OpacityProgressDone'],
                        customization['OpacityProgressToDo']]
                # Plot the bars
                ax.broken_barh(to_plot, [y_position,
                    customization['BarHeight']],
                        facecolors = colors, alpha = alpha)

                # Displaying the progress
                ax.text(task[1]['End'].iloc[-1] + dt.timedelta(
                    days = customization['ProgressTogetherPosition']),
                    y_position + customization['BarHeight']/2,
                    f"{int(round(total_progress*100, 0))}%", va = "center",
                    alpha = customization['ProgressFontAlpha'],
                    color = customization['ProgressFontColor'],
                    fontfamily = customization['ProgressFontFamily'],
                    fontsize = customization['ProgressFontSize'],
                    fontstyle = customization['ProgressFontStyle'],
                    fontweight = customization['ProgressFontWeight'])

        else:

            # Prepare data for plotting
            to_plot = list(zip(task[1]['Start'], task[1]['Task duration']))
            # Prepare a list of colors and for plotting
            colors = [customization['LegendColors'][item] for item in 
                    task[1]['Legend']]
            # Prepare a list of transparency values
            alpha = [customization['OpacityProgressDone']]*task[1].shape[0]
            # Plot the bars
            ax.broken_barh(to_plot, [y_position, customization['BarHeight']],
                    facecolors = colors, alpha = alpha)

    # Arrange major and minor tick labels
    tick_start = data['Start'].min()

    if tick_reference_date is not None:

        if tick_start > tick_reference_date:

            tick_start = tick_reference_date

    tick = tick_start
    max_tick = data['End'].max()
    x_ticks = []
    x_ticks_labels = []
    x_ticks_minor = []
    x_ticks_minor_labels = []
    delta = _time_delta(customization['PlotXStepMajor'])

    while tick < data['Start'].min():

        tick = tick + delta

    # Save the last major tick that was too early
    early_major_tick = tick - delta

    while tick <= max_tick:

        x_ticks.append(tick)
        x_ticks_labels.append(tick.strftime(
            customization['OutputDateFormat']))
        tick = tick + delta

    # Check if minor ticks are to be displayed
    if customization['PlotXStepMinor'] is not None:

        delta = _time_delta(customization['PlotXStepMinor'])

        # Check if the minor ticks should be calculated independently of the
        # major ones
        if customization['PlotXStepMinorRelativeThreshold'] is None:

            tick = tick_start

            while tick < data['Start'].min():

                tick = tick + delta

            while tick <= max_tick:

                x_ticks_minor.append(tick)
                x_ticks_minor_labels.append(tick.strftime(
                    customization['OutputDateFormat']))
                tick = tick + delta

        # If minor ticks should be calculated in reference to the major ones
        else:

            thresh_delta = _time_delta(
                    customization['PlotXStepMinorRelativeThreshold'])

            # Minor ticks before the first major tick
            tick = early_major_tick + delta
            current_thresh = x_ticks[0] - thresh_delta

            if current_thresh >= data['Start'].min() and \
                current_thresh < x_ticks[0]:

                while tick < data['Start'].min():

                    tick = tick + delta

                while tick <= current_thresh:

                    x_ticks_minor.append(tick)
                    x_ticks_minor_labels.append(tick.strftime(
                        customization['OutputDateFormat']))
                    tick = tick + delta

            # Minor ticks between the major ones
            for tick_iter in range(len(x_ticks) - 1):

                tick = x_ticks[tick_iter] + delta
                current_thresh = x_ticks[tick_iter + 1] - thresh_delta

                while tick <= current_thresh:

                    x_ticks_minor.append(tick)
                    x_ticks_minor_labels.append(tick.strftime(
                        customization['OutputDateFormat']))
                    tick = tick + delta

            # Minor ticks after the last major tick
            tick = x_ticks[-1] + delta
            current_thresh = max_tick - thresh_delta

            while tick <= current_thresh:

                x_ticks_minor.append(tick)
                x_ticks_minor_labels.append(tick.strftime(
                    customization['OutputDateFormat']))
                tick = tick + delta

    # Set ticks, hide ticks but not labels if the spine is to be hidden
    ax.set_xticks(x_ticks, color = customization['ChartColor'])
    ax.set_xticklabels(x_ticks_labels, color = customization['ChartColor'])
    ax.set_yticks(y_positions, tasks, color = customization['ChartColor'])
    ax.set_xticks(x_ticks_minor, minor = True,
            color = customization['ChartColor'])
    plt.xticks(rotation = customization['TickRotation'])

    if 'bottom' in customization['InvisibleSpines']:

        ax.xaxis.set_tick_params(length = 0)

    if 'left' in customization['InvisibleSpines']:

        ax.yaxis.set_tick_params(length = 0)

    # Change the font size on the axes
    ax.tick_params(axis = 'both', labelsize = customization['TickSize'])

    # Change the color of the spines and hide those, that are not wanted
    ax.spines[:].set_color(customization['ChartColor'])

    for spine in customization['InvisibleSpines']:

        ax.spines[spine].set_visible(False)

    # Add labels for axes and chart title, if the user defined to do so
    if customization['XAxisLabel'] is not None:

        ax.set_xlabel(customization['XAxisLabel'],
                size = customization['XAxisLabelSize'],
                color = customization['ChartColor'])

    if customization['YAxisLabel'] is not None:

        ax.set_ylabel(customization['YAxisLabel'],
                size = customization['YAxisLabelSize'],
                color = customization['ChartColor'])

    # Add plot title
    if customization['ChartTitle'] is not None:

        plt.title(customization['ChartTitle'],
                size = customization['ChartTitleSize'],
                y = customization['ChartTitleRaise'],
                color = customization['ChartColor'])

    # Add grid if the user defined to do so
    if customization['ShowGrid']:

        plt.grid(color = customization['GridColor'],
                alpha = customization['GridAlpha'],
                linestyle = customization['GridLineStyle'],
                linewidth = customization['GridLineWidth'],
                axis = customization['GridAxis'],
                which = customization['GridWhich'])
        ax.set_axisbelow(True)

    # Add categories to the legend if the user defined to do so
    legend_elements = []

    if customization['ShowCategoryLegend']:

        labels = _unique(data['Legend'].to_list())
        legend_elements = legend_elements + [
                Patch(facecolor = customization['LegendColors'][element],
                    label = element) for element in labels]

    # Adding user-defined vertical lines and preparing legend entries
    # for these lines if the user defined to do so

    for date in customization['DateLine']:

        # Add a line denoting a date if the user defined to do so

        # If 'today' keyword was used
        if date['date'] == 'today':

            # Drawing the line
            ax.axvline(dt.datetime.now(),
                    alpha = date['alpha'], color = date['color'],
                    linestyle = date['linestyle'],
                    linewidth = date['linewidth'])

            # If the line should be shown in the legend
            if date['label'] is not None:

                # If label is to be considered standard text
                if date['label'][0] == 'text':

                    legend_elements.append(Line2D([0], [0],
                        label = date['label'][1], alpha = date['alpha'],
                        color = date['color'], linestyle = date['linestyle'],
                        linewidth = date['linewidth']))

                # If label is formatting string for the date
                elif date['label'][0] == 'format':

                    try:

                        processed_date = dt.datetime.now().strftime(
                                date['label'][1])

                    except:

                        processed_date = date['label'][1]

                    legend_elements.append(Line2D([0], [0],
                        label = processed_date, alpha = date['alpha'],
                        color = date['color'], linestyle = date['linestyle'],
                        linewidth = date['linewidth']))

        # If 'today' keyword was not used
        else:

            # Try block to catch parsing errors
            try:

                # Drawing the line
                processed_date = dt.datetime.strptime(date['date'],
                        date['date_format'])
                ax.axvline(processed_date, alpha = date['alpha'],
                    color = date['color'], linestyle = date['linestyle'],
                    linewidth = date['linewidth'])

                # If the line should be shown in the legend
                if date['label'] is not None:

                    # If label is to be considered standard text
                    if date['label'][0] == 'text':

                        legend_elements.append(Line2D([0], [0],
                            label = date['label'][1], alpha = date['alpha'],
                            color = date['color'],
                            linestyle = date['linestyle'],
                            linewidth = date['linewidth']))

                    # If label is formatting string for the date
                    elif date['label'][0] == 'format':

                        try:

                            processed_date = processed_date.strftime(
                                    date['label'][1])

                        except:

                            processed_date = date['label'][1]

                        legend_elements.append(Line2D([0], [0],
                            label = processed_date, alpha = date['alpha'],
                            color = date['color'],
                            linestyle = date['linestyle'],
                            linewidth = date['linewidth']))

            except:

                pass

    # Displaying the legend
    ax.legend(handles = legend_elements, bbox_to_anchor = (1, 
        customization['LegendLocationAnchor']), loc = 'upper left',
        shadow = False, fontsize = customization['LegendFontSize'],
        facecolor = customization['LegendBackgroundColor'],
        frameon = False, labelcolor = customization['ChartColor'])

    # Invert y axis, so that 0 is on top of the plot
    ax.invert_yaxis()
    # Change plot background to white, so that all the labels of the axes,
    # ticks and title are on a white background
    fig.patch.set_facecolor(customization['ChartColorAround'])
    ax.set_facecolor(customization['ChartColorBackground'])
    # Display the plot with a tight layout, with blocking disabled, allowing
    # the script to continue with the next command
    plt.tight_layout()

    return fig


def gantt_timeline(input_path, delimiter=',', input_date_format='%Y-%m-%d',
        tick_reference_date=None, order_by_date=True, customization={}):

    """This function reads data from a CSV file, prepares it and plots a Gantt
    chart timeline based on this input data. Sane default values are used, thus
    one does not need to provide all the parameters to obtain a reasonable plot.

    Input:

        input_path: path to the .csv file containing the data (required);
        type: string;

            the input data must contain the following columns:

                Task: a column containing names of the tasks to be displayed
                on the Gantt chart;

                Start: starting date of a task;

                End: end date of a task;

            Optional columns:

                Legend: label/category of a given element for display in the
                legend of the chart;

                Progress: level of completion of a task (0 - 1);
                data: a pandas data frame containing the data for the
                Gantt chart;
        
        delimiter: delimiter for the .csv file;
        type: string; default: ',';

        input_date_format: format of data strings in the input data file,
        given using a strftime string (https://strftime.org/);
        type: string; default: '%Y-%m-%d';

        tick_reference_date: custom starting date for calculating the
        positions of the ticks; this allows to e.g. display the ticks always
        on the first day of the month, rather than every month from the
        beginning of the first task; note that the plot itself will still
        start at the beginning date of the first task - earlier ticks will
        not be displayed; the date must be given in the same format as in the
        input data, i.e. in the format given as a strftime string
        (https://strftime.org/) in input_date_format; the date must be earlier
        or the same as the earliest beginning date in the data set; if None,
        the reference date will be set to the beginning date of the first task;
        type: string or None; default: None;

        order_by_date: boolean value defining if the tasks should be ordered
        by starting date (if True) or if they should show up in the plot in the
        order they were listed in the input data (if False);
        type: boolean; default: True;

        customization: a dictionary listing all the settings for the chart;
        default: {}; available keys:

            ChartWidth: width of the chart;
            type: float; default: 20;

            ChartHeight: height of the chart;
            type: float; default: 10;

            BarGap: distance between the bars (in y direction);
            type: float; default: 0.4;

            BarHeight: height of the bars;
            type: float; default: 4;

            PlotXStepMajor: step on the x axis for the major ticks, given as
            a dictionary; not all the fields must be given, the missing ones
            will be assumed to be 0;
            type: dictionary; default: {'days': 14}; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;
            
                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            PlotXStepMinor: step on the x axis for the minor ticks, given as
            a dictionary; not all the fields must be given, the missing ones
            will be assumed to be 0; if None, no minor ticks will be displayed;
            type: dictionary or None; default: {'days': 7}; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;
            
                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            PlotXStepMinorRelativeThreshold: if set to None, minor ticks are
            calculated irrespectively of the major ones, as defined in
            PlotXStepMinor; if set to a custom dictionary value, then the
            minor ticks are calculated with respect to the preceding major
            tick, i.e. the initial date is set to be that of the preceding
            major tick and the minor steps (defined through PlotXStepMinor)
            are made as many times as possible, until the date represented by
            the next major tick is reached; at that point the date of the new
            major tick is taken as the new starting point for the next minor
            ticks; the procedure repeats until the final date is reached (the
            threshold apply to it as well); minor ticks preceding the first
            major tick are also calculated according to the rules, with the
            first one starting not earlier than the earliest starting date
            in the data set; this allows e.g. displaying a minor tick every
            week of the month, starting at the beginning of the month; the
            dictionary enables customization of the number of minor ticks to
            be displayed by defining an upper threshold for the minor ticks
            to be displayed; the time delta defined by the dictionary is
            subtracted from the date represented by the next major tick or
            the final date in the set; if the date for the new minor tick is
            earlier than or the same as that calculated date, the tick will
            be displayed; otherwise it will be omitted; this allows to e.g.
            display a minor tick each 15th of the month, without a tick on
            29th; not all the fields of the dictionary must be given, the
            missing ones will be assumed to be 0 (note that an empty
            dictionary might be passed - in such a case all the minor ticks
            will be displayed, with the threshold being equal to the date of
            the next major tick);
            type: dictionary; default: None; allowed fields:

                'years': number of years; type: float;

                'months': number of months; type: float;
            
                'weeks': number of weeks; type: float;

                'days': number of days; type: float;

                'hours': number of hours; type: float;

                'minutes': number of minutes; type: float;

                'seconds': number of seconds; type: float;

                'microseconds': number of microseconds; type: float;

            XAxisLabel: label for the x axis, None if not to be displayed;
            type: string or None; default: None;

            XAxisLabelSize: size of the x axis label if given;
            type: any recognized by matplotlib; default: 32;

            YAxisLabel: label for the y axis, None if not to be displayed;
            type: string or None; default: None;

            YAxisLabelSize: size of the y axis label if given;
            type: any recognized by matplotlib; default: 32;

            TickSize: size of the tick labels on both axes;
            type: any recognized by matplotlib; default: 24;

            TickRotation: rotation of the tick labels on x axis;
            type: any recognized by matplotlib; default: 60;

            ChartTitle: title of the chart, None if not to be displayed;
            type: string or None; default: None;

            ChartTitleSize: size of the title of the chart, if given;
            type: any recognized by matplotlib; default: 34;

            ChartTitleRaise: a parameter that defines how high the chart title
            is raised above the plot;
            type: float; default: 1.03;

            ShowProgress: decide whether to display the progress of the task,
            both by changing the opacity of the completed tasks as well as
            displaying the value of progress in %;
            type: bool; default: False;

            ProgressSeparateDefault: if progress displayed and there are
            multiple tasks with the same label, define whether by default
            the progress for each of them should be displayed separately or
            considering them all together; in the latter case the total
            progress will be calculated from the individual values of progress
            of each of the task;
            type: bool; default: False;

            ProgressSeparate: if progress displayed and there are multiple
            tasks with the same label, define for each label whether to display
            the progress for individual tasks separately or considering them
            all together; in the latter case the total progress will be
            calculated from the individual values of progress of each of the
            task; default behaviour can be set with ProgressSeparateDefault;
            this way only exceptions can be given through this parameter;
            type: dict the keys of which are task labels and the value is bool
            defining whether progress of individual tasks using the same label
            should be considered separately (True) or should the grouped
            progress be displayed (False); default: as set through the
            customization dict variable ProgressSeparateDefault;

            ProgressFontAlpha: opacity for the font of the text informing about
            task progress;
            type: float (0 - 1); default: 1;

            ProgressFontColor: color of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'black';

            ProgressFontFamily: family of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'serif';

            ProgressFontSize: size of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 18;

            ProgressFontStyle: style of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'normal';
            
            ProgressFontWeight: weight of the font of the text informing about
            task progress;
            type: any recognized by matplotlib; default: 'normal';

            OpacityProgressDone: opacity of the parts of the bars that
            reperesent parts of the tasks that have been completed;
            type: float (0 - 1); default: 1;

            OpacityProgressToDo: opacity of the parts of the bars that
            represent parts of the tasks that are yet to be completed;
            type: float(0 - 1); default: 0.5;

            ShowCategoryLegend: decide whether to display a legend based on
            categories assigned to tasks; works only if a 'Legend' column
            was provided;
            type: bool; default: False;

            LegendLocationAnchor: location (in y) of the bounding box for the
            legend on the chart;
            type: any recognized by matplotlib; default: 1.0085;

            LegendFontSize: size of the font in the legend;
            type: any recognized by matplotlib; default: 24;

            LegendColors: a dictionary, the keys of which are categories
            that can be assigned to tasks and the values of which are colors
            of bars (any format recognized by matplotlib); note that this
            variable may be used for arbitrary designation of colors of bars
            without displaying the legend;
            type: dictionary of colors (any format recognized by matplotlib);
            default: dictionary of all the legend labels + 'default', with
            the color "royalblue" assigned to all of them;

            LegendBackgroundColor: color of the background of the legend;
            type: any recognized by matplotlib; default: 'snow';

            ChartColorAround: the color of the background around the chart;
            type: any recognized by matplotlib; default: 'snow';

            ChartColorBackground: the color of the background of the chart
            (plotting field);
            type: any recognized by matplotlib; default: 'snow';

            ChartColor: the color of the fonts of the chart, spines, ticks,
            tick labels, axes' labels, legend elements etc.;
            type: any recognized by matplotlib; default: 'black';

            ShowGrid: define whether a grid should be displayed on the chart;
            type: bool; default: True;

            GridColor: color of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: 'gainsboro';

            GridAlpha: opacity of the grid lines;
            type: float (0 - 1); default: 0.5;

            GridLineStyle: style of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: '-';

            GridLineWidth: width of the grid to be displayed in the chat;
            type: any recognized by matplotlib; default: 1;

            GridWhich: choice of grid lines with respect to ticks;
            type: string ('major', 'minor', 'both'); default: 'major';

            GridAxis: choice of grid lines (horizontal, vertical, both);
            type: string ('both', 'x', 'y'); default: 'x';

            ProgressTogetherPosition: horizontal gap between the bar and the
            text describing the progress of a task in days;
            type: float; default: 1.5;

            OutputDateFormat: format of data strings to be used in the chart,
            given using a strftime string (https://strftime.org/);
            type: string; default: '%d %B';

            DateLine: a list of dictionaries detailing the vertical lines to be
            displayed on the chart;
            type: dictionary; default: {}; allowed fields:

                'date': a date to mark on the chart with a vertical line; if
                "today" is passed as an argument, the date at which the script
                is run will be used; otherwise, it can be any string that can
                be parsed using a strftime string given in 'date_format' field;
                type: string;

                'date_format': format for the date given in 'date', given
                using a strftime string (https://strftime.org/);
                type: string;

                'label': a two element list that defines if and how the line
                should be represented in the legend; the first element can be
                'text' or 'format' - it defines whether the second field
                should be interpreted as plain text or as a format in which the
                date should be displayed in the legend; the second string is
                either text label to be displayed in the legend or a format
                for the date to be shown; if a given vertical line should not
                be represented in the legend, None is passed instead of the
                described list;
                type: 2-element list of strings or None;

                'alpha': opacity of the line to be plotted;
                type: float (0 - 1);

                'color': color of the line to be plotted;
                type: any recognized by matplotlib;

                'linestyle': line style of the line to be plotted;
                type: any recognized by matplotlib; default = '-';

                'linewidth': width of the line of the line to be plotted;
                type: any recognized by matplotlib; default = 2;

            InvisibleSpines: list of chart spines that should be hidden;
            type: list of strings ('top', 'bottom', 'left', 'right');
            default: ['top', 'left', 'right'].

        Output: 

            fig: matplotlib figure with the Gantt chart;
            type: matplotlib figure.
"""

    # Read data from a CSV file and prepare the data frame for further work
    data, order = _read_input(input_path, delimiter, input_date_format,
                              order_by_date)

    # Convert tick_reference_date to datetime object if it is not None
    if tick_reference_date is not None:

        tick_reference_date = dt.datetime.strptime(tick_reference_date,
                                                   input_date_format)

    # Further process the data and prepare the plot
    fig = _plot_gantt(data, order, tick_reference_date, customization)

    return fig
