# Imports
import sys
import matplotlib.pyplot as plt
# Add module to path and import it
sys.path.insert(0, '../')
import gantt_timeline_generator as gtg

# List of dates to be marked with vertical lines on the chart
dates = [
        {'date': '12-05-2022',
         'date_format': '%d-%m-%Y',
         'label': ['format', '%d %B %y'],
         'alpha': 1,
         'color': 'firebrick',
         'linestyle': '-',
         'linewidth': 2,
        },
        {'date': '2022-01-30',
         'date_format': '%Y-%m-%d',
         'label': ['text', 'test label'],
         'alpha': 0.8,
         'color': 'mediumseagreen',
         'linestyle': '--',
         'linewidth': 4,
        }
        ]

# Colors for categories                                                                             
legend_colors = {                                                                                    
        'category A': 'royalblue',                                                                       
        'category B': 'orangered',                                                                          
        'category C': 'gold',                                                                                
        }

# Defining for which tasks progress should be shown separately.
# Note that this works with 'ProgressSeperateDefaul' from the
# customization dictionary. If you do not specify certain
# tasks here, a default value given by the aforementioned
# parameter will be assigned to them.
separate_progress = {
        'E': True,
        }

# Dictionary of customization values
customization = {
        'ChartWidth': 18, 
        'ChartHeight': 9,
        'BarGap': 0.2, 
        'BarHeight': 3,
        'PlotXStepMajor': {'months': 1},
        'PlotXStepMinor': {'weeks': 2},
        'PlotXStepMinorRelativeThreshold': {'days': 4},
        'XAxisLabel': 'X',  
        'XAxisLabelSize': 32,
        'YAxisLabel': 'Y',  
        'YAxisLabelSize': 29,
        'TickSize': 25,    
        'TickRotation': 55,
        'ChartTitle': 'Test title',  
        'ChartTitleSize': 36,   
        'ChartTitleRaise': 1.02,
        'ShowProgress': True,
        'ProgressSeparate': separate_progress,
        'ProgressSeparateDefault': False,
        'ProgressFontAlpha': 1,
        'ProgressFontColor': 'black',
        'ProgressFontFamily': 'serif',
        'ProgressFontSize': 18,
        'ProgressFontStyle': 'normal',
        'ProgressFontWeight': 'normal',
        'OpacityProgressDone': float(1.0),
        'OpacityProgressToDo': 0.5,
        'ShowCategoryLegend': True,
        'LegendLocationAnchor': 1.0085,
        'LegendFontSize': 24,
        'LegendColors': legend_colors,
        'LegendBackgroundColor': 'snow',
        'ChartColorAround': 'snow',
        'ChartColorBackground': 'snow',
        'ChartColor': 'black',
        'ShowGrid': True,
        'GridColor': 'gainsboro',
        'GridAlpha': 0.5,
        'GridLineStyle': '--',
        'GridLineWidth': 0.5,
        'GridWhich': 'both',
        'GridAxis': 'both',
        'ProgressTogetherPosition': 1.5,
        'OutputDateFormat': '%d %B',
        'DateLine': dates,
        'InvisibleSpines': ['top', 'left', 'right'],
        }

# Data input path, input data date format and delimiter in the CSV file
input_path = './general_test_data.csv'
input_date_format = '%d-%m-%Y'
delimiter = ','
# Toggling ordering of the items by date
order_by_date = True
# Reference date for the ticks
tick_reference_date = '01-01-2022'

# Preparing the plot
fig = gtg.gantt_timeline(input_path, delimiter=delimiter,
        input_date_format=input_date_format, order_by_date=order_by_date,
        customization=customization)
plt.show()
