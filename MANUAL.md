# MANUAL

This is the manual for the [Gantt Chart Timeline Generator](README.md) tool. It contains information about installation of the tool as well as documentation for each of the user-facing functions, explaining all the variables that can be used to control the output of the tool. Note that you can access this function documentation in the interactive Python session by typing help(function_name).

## Installation

### Requirements

Dependencies: matplotlib, pandas. The tool was tested and is confirmed to work with Python 3.9.7, matplotlib 3.5.1 and pandas 1.4.1.

### Installation

1. Clone this repository to any directory you see fit.
2. Place the gantt_timeline_generator.py file in a directory at your system path or, in your Python script, add the directory where gantt_timeline_generator.py is located to your system path. The latter can be done by inserting the following commands at the beginning of your Python script:
> 
> ```
> import sys
> import matplotlib.pyplot as plt
> # Replace '../' with the path to the directory
> # contatining gantt_timeline_generator.py
> sys.path.insert(0, '../')
> import gantt_timeline_generator as gtg
> ```

## Language support

[Gannt Chart Timeline Generator](README.md) uses system locales to determine the formatting for the output (e.g. names of the months). If you want to use different locales, make sure that they are installed on your system and switch to them. Please check how to do this on your platform. On Linux you can check your currently set locales by running:

>
> ```
> locales
> ```
in your terminal. To check available locales you can run:
>
> ```
> locales -a
> ```
To execute the script in which you use the [Gannt Chart Timeline Generator](README.md) with temporarily selected locales you can run:
>
> ```
> LC_ALL="en_GB.utf8" python chart.py
> ```
where ```en_GB.utf8``` is some name from the list displayed after running ```locales -a``` and ```chart.py``` is the name of the script.

## Functions

### gantt_timeline(*input_path, delimiter=',', input_date_format='%Y-%m-%d', tick_reference_date=None, order_by_date=True, customization={}*)

This function reads data from a CSV file, prepares it and plots a Gantt
chart timeline based on this input data. Sane default values are used, thus
one does not need to provide all the parameters to obtain a reasonable plot.

Input:

***input_path***: path to the .csv file containing the data (required);
type: string;

> The input data must contain the following columns:

> > ***Task***: a column containing names of the tasks to be displayed
> > on the Gantt chart;

> > ***Start***: starting date of a task;

> > ***End***: end date of a task;

> Optional columns:

> > ***Legend***: label/category of a given element for display in the
> > legend of the chart;

> > ***Progress***: level of completion of a task (0 - 1);
> > data: a pandas data frame containing the data for the
> > Gantt chart;

***delimiter***: delimiter for the .csv file;
type: string; default: ',';

***input_date_format***: format of data strings in the input data file,
given using a strftime string (https://strftime.org/);
type: string; default: '%Y-%m-%d';

***tick_reference_date***: custom starting date for calculating the
positions of the ticks; this allows to e.g. display the ticks always
on the first day of the month, rather than every month from the
beginning of the first task; note that the plot itself will still
start at the beginning date of the first task - earlier ticks will
not be displayed; the date must be given in the same format as in the
input data, i.e. in the format given as a strftime string
(https://strftime.org/) in input_date_format; the date must be earlier
or the same as the earliest beginning date in the data set; if None,
the reference date will be set to the beginning date of the first task;
type: string or None; default: None;

***order_by_date***: boolean value defining if the tasks should be ordered by starting date (if True) or if they should show up in the plot in the order they were listed in the input data (if False);
type: boolean; default: True;

***customization***: a dictionary listing all the settings for the chart;
default: {}; available keys:

> ***ChartWidth***: width of the chart;
> type: float; default: 20;

> ***ChartHeight***: height of the chart;
> type: float; default: 10;

> ***BarGap***: distance between the bars (in y direction);
> type: float; default: 0.4;

> ***BarHeight***: height of the bars;
> type: float; default: 4;

> ***PlotXStepMajor***: step on the x axis for the major ticks, given as
> a dictionary; not all the fields must be given, the missing ones
> will be assumed to be 0;
> type: dictionary; default: {'days': 14}; allowed fields:

> > ***'years'***: number of years; type: float;

> > ***'months'***: number of months; type: float;
> 
> > ***'weeks'***: number of weeks; type: float;

> > ***'days'***: number of days; type: float;

> > ***'hours'***: number of hours; type: float;

> > ***'minutes'***: number of minutes; type: float;

> > ***'seconds'***: number of seconds; type: float;

> > ***'microseconds'***: number of microseconds; type: float;

> ***PlotXStepMinor***: step on the x axis for the minor ticks, given as
> a dictionary; not all the fields must be given, the missing ones
> will be assumed to be 0;
> type: dictionary; default: {'days': 7}; allowed fields:

> > ***'years'***: number of years; type: float;

> > ***'months'***: number of months; type: float;
> 
> > ***'weeks'***: number of weeks; type: float;

> > ***'days'***: number of days; type: float;

> > ***'hours'***: number of hours; type: float;

> > ***'minutes'***: number of minutes; type: float;

> > ***'seconds'***: number of seconds; type: float;

> > ***'microseconds'***: number of microseconds; type: float;

> ***PlotXStepMinorRelativeThreshold***: if set to None, minor ticks are
  calculated irrespectively of the major ones, as defined in
  PlotXStepMinor; if set to a custom dictionary value, then the
  minor ticks are calculated with respect to the preceding major
  tick, i.e. the initial date is set to be that of the preceding
  major tick and the minor steps (defined through PlotXStepMinor)
  are made as many times as possible, until the date represented by
  the next major tick is reached; at that point the date of the new
  major tick is taken as the new starting point for the next minor
  ticks; the procedure repeats until the final date is reached (the
  threshold apply to it as well); minor ticks preceding the first
  major tick are also calculated according to the rules, with the
  first one starting not earlier than the earliest starting date
  in the data set; this allows e.g. displaying a minor tick every
  week of the month, starting at the beginning of the month; the
  dictionary enables customization of the number of minor ticks to
  be displayed by defining an upper threshold for the minor ticks
  to be displayed; the time delta defined by the dictionary is
  subtracted from the date represented by the next major tick or
  the final date in the set; if the date for the new minor tick is
  earlier than or the same as that calculated date, the tick will
  be displayed; otherwise it will be omitted; this allows to e.g.
  display a minor tick each 15th of the month, without a tick on
  29th; not all the fields of the dictionary must be given, the
  missing ones will be assumed to be 0 (note that an empty
  dictionary might be passed - in such a case all the minor ticks
  will be displayed, with the threshold being equal to the date of
  the next major tick);
  type: dictionary; default: None; allowed fields:

> > ***'years'***: number of years; type: float;

> > ***'months'***: number of months; type: float;
> 
> > ***'weeks'***: number of weeks; type: float;

> > ***'days'***: number of days; type: float;

> > ***'hours'***: number of hours; type: float;

> > ***'minutes'***: number of minutes; type: float;

> > ***'seconds'***: number of seconds; type: float;

> > ***'microseconds'***: number of microseconds; type: float;

> ***XAxisLabel***: label for the x axis, None if not to be displayed;
> type: string or None; default: None;

> ***XAxisLabelSize***: size of the x axis label if given;
> type: any recognized by matplotlib; default: 32;

> ***YAxisLabel***: label for the y axis, None if not to be displayed;
> type: string or None; default: None;

> ***YAxisLabelSize***: size of the y axis label if given;
> type: any recognized by matplotlib; default: 32;

> ***TickSize***: size of the tick labels on both axes;
> type: any recognized by matplotlib; default: 24;

> ***TickRotation***: rotation of the tick labels on x axis;
> type: any recognized by matplotlib; default: 60;

> ***ChartTitle***: title of the chart, None if not to be displayed;
> type: string or None; default: None;

> ***ChartTitleSize***: size of the title of the chart, if given;
> type: any recognized by matplotlib; default: 34;

> ***ChartTitleRaise***: a parameter that defines how high the chart title
> is raised above the plot;
> type: float; default: 1.03;

> ***ShowProgress***: decide whether to display the progress of the task,
> both by changing the opacity of the completed tasks as well as
> displaying the value of progress in %;
> type: bool; default: False;

> ***ProgressSeparateDefault***: if progress displayed and there are
> multiple tasks with the same label, define whether by default
> the progress for each of them should be displayed separately or
> considering them all together; in the latter case the total
> progress will be calculated from the individual values of progress
> of each of the task;
> type: bool; default: False;

> ***ProgressSeparate***: if progress displayed and there are multiple
> tasks with the same label, define for each label whether to display
> the progress for individual tasks separately or considering them
> all together; in the latter case the total progress will be
> calculated from the individual values of progress of each of the
> task; default behaviour can be set with ProgressSeparateDefault;
> this way only exceptions can be given through this parameter;
> type: dict the keys of which are task labels and the value is bool
> defining whether progress of individual tasks using the same label
> should be considered separately (True) or should the grouped
> progress be displayed (False); default: as set through the
> customization dict variable ProgressSeparateDefault;

> ***ProgressFontAlpha***: opacity for the font of the text informing about
> task progress;
> type: float (0 - 1); default: 1;

> ***ProgressFontColor***: color of the font of the text informing about
> task progress;
> type: any recognized by matplotlib; default: 'black';

> ***ProgressFontFamily***: family of the font of the text informing about
> task progress;
> type: any recognized by matplotlib; default: 'serif';

> ***ProgressFontSize***: size of the font of the text informing about
> task progress;
> type: any recognized by matplotlib; default: 18;

> ***ProgressFontStyle***: style of the font of the text informing about
> task progress;
> type: any recognized by matplotlib; default: 'normal';
> 
> ***ProgressFontWeight***: weight of the font of the text informing about
> task progress;
> type: any recognized by matplotlib; default: 'normal';

> ***OpacityProgressDone***: opacity of the parts of the bars that
> reperesent parts of the tasks that have been completed;
> type: float (0 - 1); default: 1;

> ***OpacityProgressToDo***: opacity of the parts of the bars that
> represent parts of the tasks that are yet to be completed;
> type: float(0 - 1); default: 0.5;

> ***ShowCategoryLegend***: decide whether to display a legend based on
> categories assigned to tasks; works only if a 'Legend' column
> was provided;
> type: bool; default: False;

> ***LegendLocationAnchor***: location (in y) of the bounding box for the
> legend on the chart;
> type: any recognized by matplotlib; default: 1.0085;

> ***LegendFontSize***: size of the font in the legend;
> type: any recognized by matplotlib; default: 24;

> ***LegendColors***: a dictionary, the keys of which are categories
> that can be assigned to tasks and the values of which are colors
> of bars (any format recognized by matplotlib); note that this
> variable may be used for arbitrary designation of colors of bars
> without displaying the legend;
> type: dictionary of colors (any format recognized by matplotlib);
> default: dictionary of all the legend labels + 'default', with
> the color "royalblue" assigned to all of them;

> ***LegendBackgroundColor***: color of the background of the legend;
> type: any recognized by matplotlib; default: 'snow';

> ***ChartColorAround***: the color of the background around the chart;
> type: any recognized by matplotlib; default: 'snow';

> ***ChartColorBackground***: the color of the background of the chart
> (plotting field);
> type: any recognized by matplotlib; default: 'snow';

> ***ChartColor***: the color of the fonts of the chart, spines, ticks,
> tick labels, axes' labels, legend elements etc.;
> type: any recognized by matplotlib; default: 'black';

> ***ShowGrid***: define whether a grid should be displayed on the chart;
> type: bool; default: True;

> ***GridColor***: color of the grid to be displayed in the chat;
> type: any recognized by matplotlib; default: 'gainsboro';

> ***GridAlpha***: opacity of the grid lines;
> type: float (0 - 1); default: 0.5;

> ***GridLineStyle***: style of the grid to be displayed in the chat;
> type: any recognized by matplotlib; default: '-';

> ***GridLineWidth***: width of the grid to be displayed in the chat;
> type: any recognized by matplotlib; default: 1;

> ***GridWhich***: choice of grid lines with respect to ticks;
> type: string ('major', 'minor', 'both'); default: 'major';

> ***GridAxis***: choice of grid lines (horizontal, vertical, both);
> type: string ('both', 'x', 'y'); default: 'x';

> ***ProgressTogetherPosition***: horizontal gap between the bar and the
> text describing the progress of a task in days;
> type: float; default: 1.5;

> ***OutputDateFormat***: format of data strings to be used in the chart,
> given using a strftime string (https://strftime.org/);
> type: string; default: '%d %B';

> ***DateLine***: a list of dictionaries detailing the vertical lines to be
> displayed on the chart;
> type: dictionary; default: {}; allowed fields:

> > ***'date'***: a date to mark on the chart with a vertical line; if
> > "today" is passed as an argument, the date at which the script
> > is run will be used; otherwise, it can be any string that can
> > be parsed using a strftime string given in 'date_format' field;
> > type: string;

> > ***'date_format'***: format for the date given in 'date', given
> > using a strftime string (https://strftime.org/);
> > type: string;

> > ***'label'***: a two element list that defines if and how the line
> > should be represented in the legend; the first element can be
> > 'text' or 'format' - it defines whether the second field
> > should be interpreted as plain text or as a format in which the
> > date should be displayed in the legend; the second string is
> > either text label to be displayed in the legend or a format
> > for the date to be shown; if a given vertical line should not
> > be represented in the legend, None is passed instead of the
> > described list;
> > type: 2-element list of strings or None;

> > ***'alpha'***: opacity of the line to be plotted;
> > type: float (0 - 1);

> > ***'color'***: color of the line to be plotted;
> > type: any recognized by matplotlib;

> > ***'linestyle'***: line style of the line to be plotted;
> > type: any recognized by matplotlib; default = '-';

> > ***'linewidth'***: width of the line of the line to be plotted;
> > type: any recognized by matplotlib; default = 2;

> ***InvisibleSpines***: list of chart spines that should be hidden;
> type: list of strings ('top', 'bottom', 'left', 'right');
> default: ['top', 'left', 'right'].

Output: 

***fig***: matplotlib figure with the Gantt chart;
type: matplotlib figure.
